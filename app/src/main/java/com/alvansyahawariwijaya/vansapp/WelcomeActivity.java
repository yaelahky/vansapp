package com.alvansyahawariwijaya.vansapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class WelcomeActivity extends AppCompatActivity {
    TextView eName, eEmail, eWeb;
    String oldName, oldEmail, oldWebsite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        eEmail = findViewById(R.id.tv_email);
        eName = findViewById(R.id.tv_name);
        eWeb = findViewById(R.id.tv_web);

        Intent resultIntent = getIntent();
        oldName = resultIntent.getStringExtra("nameRegister");
        oldEmail = resultIntent.getStringExtra("emailRegister");
        oldWebsite = resultIntent.getStringExtra("webRegister");

        eName.setText(oldName);
        eEmail.setText(oldEmail);
        eWeb.setText(oldWebsite);

        final Intent intentHome = new Intent(this, Home.class);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intentHome);
                finish();
            }
        }, 2500);
    }
}
