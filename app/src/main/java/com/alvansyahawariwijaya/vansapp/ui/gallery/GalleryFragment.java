package com.alvansyahawariwijaya.vansapp.ui.gallery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.alvansyahawariwijaya.vansapp.R;

import java.util.ArrayList;
import java.util.Objects;

public class GalleryFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);


        ListView listView = (ListView) view.findViewById(R.id.listView);
        ArrayList<ModelContact> listContact = new ArrayList<>();

        ModelContact m1;

        m1 = new ModelContact(R.drawable.imag1, "kiky", "kiky.lenovo@gmail.com", "08223150000", "https://www.kiky.com");

        listContact.add(m1);
        listView.setAdapter(new CustomAdapter(getActivity(), R.layout.item_contact, listContact));



        return view;
    }

}
