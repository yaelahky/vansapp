package com.alvansyahawariwijaya.vansapp.ui.gallery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alvansyahawariwijaya.vansapp.R;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<ModelContact> {

    private List<ModelContact> listContact;
    private Context mContext;

    static class ContactHolder {
        ImageView fotoContact, iconEmail, iconPhone, iconWebsite;
        TextView nameContact, emailContact, phoneContact, webContact;
    }

    public CustomAdapter(@NonNull Context context, int resource, @NonNull List<ModelContact> objects) {
        super(context, R.layout.item_contact, objects);
        this.mContext = context;
        this.listContact = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        ContactHolder holder;

        if (v == null){
            LayoutInflater li = ((Activity)mContext).getLayoutInflater();
            v = li.inflate(R.layout.item_contact, parent, false);
            holder = new ContactHolder();
            holder.fotoContact = v.findViewById(R.id.iv_contact);
            holder.nameContact = v.findViewById(R.id.tv_name);
            holder.emailContact = v.findViewById(R.id.tv_email);
            holder.webContact = v.findViewById(R.id.tv_web);
            holder.phoneContact = v.findViewById(R.id.tv_phone);

            holder.iconEmail = v.findViewById(R.id.icon_email);
            holder.iconPhone = v.findViewById(R.id.icon_telpon);
            holder.iconWebsite = v.findViewById(R.id.icon_web);

            v.setTag(holder);
        } else {
            holder = (ContactHolder) v.getTag();
        }

        final ModelContact contact = listContact.get(position);
        holder.fotoContact.setImageResource(contact.getmImage());
        holder.nameContact.setText(contact.getName());
        holder.emailContact.setText(contact.getEmail());
        holder.webContact.setText(contact.getWebsite());
        holder.phoneContact.setText(contact.getPhoneNumber());

        holder.iconEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, contact.getEmail());
                // intent.putExtra(Intent.EXTRA_SUBJECT, "Subject"); // if you want extra
                // intent.putExtra(Intent.EXTRA_TEXT, "I'm email body."); // if you want extra

                ((Activity)mContext).startActivity(intent);
            }
        });

        holder.iconPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                String phoneNumber = "tel:"+contact.getPhoneNumber();
                intent.setData(Uri.parse(phoneNumber));

                ((Activity)mContext).startActivity(intent);
            }
        });

        holder.iconWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = contact.getWebsite();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));

                ((Activity)mContext).startActivity(intent);
            }
        });

        return v;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
