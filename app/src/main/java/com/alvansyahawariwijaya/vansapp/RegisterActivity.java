package com.alvansyahawariwijaya.vansapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity {

    EditText eName, eEmail, ePassword, eWeb;
    String nameText, emailText, webText;

    String oldEmail, oldPassword;

    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        eEmail = findViewById(R.id.et_email);
        ePassword = findViewById(R.id.et_password);
        eWeb = findViewById(R.id.et_web);
        eName = findViewById(R.id.et_name);

        Intent resultIntent = getIntent();
        oldEmail = resultIntent.getStringExtra("emailLogin");
        oldPassword = resultIntent.getStringExtra("passwordLogin");

        eEmail.setText(oldEmail);
        ePassword.setText(oldPassword);

        final Intent intentRegister = new Intent(this, WelcomeActivity.class);
        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailText = eEmail.getText().toString();
                webText = eWeb.getText().toString();
                nameText = eName.getText().toString();

                intentRegister.putExtra("emailRegister", emailText);
                intentRegister.putExtra("webRegister", webText);
                intentRegister.putExtra("nameRegister", nameText);

                startActivity(intentRegister);
                finish();
            }
        });
    }
}
