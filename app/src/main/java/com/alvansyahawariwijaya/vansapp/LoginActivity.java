package com.alvansyahawariwijaya.vansapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {

    EditText eEmail, ePassword;
    String emailText, passwordText;
    Button btnRegister, btnLogin;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnRegister = findViewById(R.id.btn_register);
        btnLogin = findViewById(R.id.btn_login);
        eEmail = findViewById(R.id.et_email);
        ePassword = findViewById(R.id.et_password);

        final Intent intentRegister = new Intent(this, RegisterActivity.class);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendParam(intentRegister);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendParam(intentRegister);
            }
        });
    }

    public void sendParam(Intent intentRegister) {
        emailText = eEmail.getText().toString();
        passwordText = ePassword.getText().toString();

        intentRegister.putExtra("emailLogin", emailText);
        intentRegister.putExtra("passwordLogin", passwordText);

        startActivity(intentRegister);
    }
}
